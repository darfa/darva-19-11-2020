<?php
	class M_kecamatan extends CI_model{

	public function select($where = ""){
		$data= $this->db->query("select * from data_kecamatan ".$where);
		return $data;
		
	}
	function getKec(){
        return $this->db->get('data_kota');
    }
	public function cekkodebarang(){
        $query = $this->db->query("SELECT MAX(id_kecamatan) as id_kecamatan from data_kecamatan");
        $hasil = $query->row();
        return $hasil->id_kecamatan;
    }
  	
	public function add($data){
			$this->db->insert('data_kecamatan',$data);
	}

	public function ubah($data){
		$this->db->where('id_kecamatan',$data['id_kecamatan']);
		$this->db->update('data_kecamatan',$data);
	}
	public function hapus($table,$where){
		return $this->db->delete($table,$where);
	}
}

?>