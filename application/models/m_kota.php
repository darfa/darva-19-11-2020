<?php
	class M_kota extends CI_model{

	public function select($where = ""){
		$data= $this->db->query("select * from data_kota ".$where);
		return $data;
		
	}
	function getKota()
    {
        return $this->db->get('data_kota');
    }
    function get_sub_kota($kota){
        $query = $this->db->get_where('data_kecamatan', array('subkota_id' => $kota));
        return $query;
    }
	public function add($data){
			$this->db->insert('data_kota',$data);
	}

	public function ubah($data){
		$this->db->where('id_kota',$data['id_kota']);
		$this->db->update('data_kota',$data);
	}
	public function hapus($table,$where){
		return $this->db->delete($table,$where);
	}
}

?>