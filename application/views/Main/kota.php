<html lang="en">
<?php include "header.php"; ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/extra-libs/multicheck/multicheck.css')?>">
<link href="<?php echo base_url('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet')?>">
<link href="<?php echo base_url('assets/dist/css/style.min.css" rel="stylesheet')?>">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                    <h5 class="card-title">Data Kota</h5>
                <div class="table-responsive">
            <table id="zero_config" class="table table-striped table-bordered" id="mydata">
                <?= $this->session->flashdata('pesan'); ?>
        <thead>
            <tr align="center">
                <th>Id Kota</th>
                <th>Kota/Kabupaten</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($data_kota as $key => $values){
                    echo "
                        <tr  align='center'>
                        <td>".$data_kota[$key]->id_kota."</td>                     
                        <td>".$data_kota[$key]->kota."</td>                   
                    ";?>
                <td align="center">
                <a class="btn btn-info btn-us" title="ubah" href="<?php echo base_url();?>C_kota/edit/<?php echo $data_kota[$key]->id_kota;?>"><span class="fa fa-edit"></span></a>
                <a class="btn btn-danger btn-us" title="hapus" onclick="return confirm('Ingin menghapus Data?');" href="<?php echo base_url();?>C_kota/delete/<?php echo $data_kota[$key]->id_kota;?>"><span class ="fa fa-trash"></span></a>
                </td>
                <?php echo "</tr>";
                }   
            ?>
        </tbody>
    </table>
    <!--modal input-->
        <p><a class="btn btn-success" data-toggle="modal" data-target="#modal_add_new"><i class="fa fa-plus"></i>Tambah Kota</a>
        </div>
    </div>
</div>
<footer class="footer text-center">
                All Rights Reserved by Darva. <a href="https://instagram.com/dsadas/">Instagram Me</a>.
            </footer>
<div class="modal fade" id="modal_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel">Tambah Kota</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url('C_kota/proses_tambah');?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Id kota</label>
                        <div class="col-xs-8">
                            <input type="number" name="id_kota" class="form-control" placeholder="Id kota" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Kota/Kabupaten</label>
                        <div class="col-xs-8">
                            <input type="text" name="kota" class="form-control" placeholder="Kota/Kabupaten" required>
                        </div>
                    </div>
                </div>
 
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" name="submit">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
    <script src="<?php echo base_url('assets/extra-libs/multicheck/datatable-checkbox-init.js')?>"></script>
    <script src="<?php echo base_url('assets/extra-libs/multicheck/jquery.multicheck.js')?>"></script>
    <script src="<?php echo base_url('assets/extra-libs/DataTables/datatables.min.js')?>"></script>
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#zero_config').DataTable();
    </script>
</html>