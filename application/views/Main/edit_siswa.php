<!DOCTYPE html>
<?php include "header.php"; ?>
<html>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/libs/select2/dist/css/select2.min.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/libs/jquery-minicolors/jquery.minicolors.css')?>">
<body>
<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <form class="form-horizontal" method="POST" 
                            action="<?php echo base_url('c_siswa/update');?>">
                                <div class="card-body">
                                    <h4 class="card-title">Edit Pemesanan</h4>
                                    <div class="form-group row">
                                        <input type="hidden" name="status_peng" value="1">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Id Siswa</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="id_siswa" class="form-control" value="<?php echo sprintf("%04s", $id_siswa) ?>" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Nama Siswa</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nama_siswa" name="nama_siswa" value="<?php echo $nama_siswa; ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Wilayah" class="col-sm-3 text-right control-label col-form-label">Kota/Kabupaten</label>
                                        <div class="col-sm-9">
                                            <select name="data_kota" id="data_kota"  class="form-control" required>
                                            <option value="0">-Pilih-</option>
                                            <?php foreach($data_kota as $data){ ?>
                                            <option value="<?= $data->id_kota?>"><?= $data->kota; ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                        </div>
                                    <div class="form-group row">
                                        <label for="data_kecamatan" class="col-sm-3 text-right control-label col-form-label">Kecamatan</label>
                                        <div class="col-sm-9">
                                        <select name="data_kecamatan" id="data_kecamatan" class="form-control">
                                               <option value="0">-Pilih-</option>
                                          </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <input type="textarea"class="form-control" id="alamat" name="alamat" value="<?php echo $alamat; ?>">
                                        </div>
                                    </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">Ubah</button>
                                        <button type="button" value="Go Back" onclick="history.back(-1)" class="btn btn-warning">Batal</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </body>
                    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
    <script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
 
            $('#data_kota').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('c_siswa/get_sub_kota');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id_kecamatan+'>'+data[i].kecamatan+'</option>';
                        }
                        $('#data_kecamatan').html(html);
                        
                    }

                });
                return false;
            });
        });
    </script>
    <script src="<?php echo base_url('assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js')?>"></script>
    <script src="<?php echo base_url('assets/dist/js/pages/mask/mask.init.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/select2/dist/js/select2.full.min.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/select2/dist/js/select2.min.js')?>"></script>
</html>