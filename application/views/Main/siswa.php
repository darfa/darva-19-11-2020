<html lang="en">
<?php include "header.php"; ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/extra-libs/multicheck/multicheck.css')?>">
<link href="<?php echo base_url('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet')?>">
<link href="<?php echo base_url('assets/dist/css/style.min.css" rel="stylesheet')?>">

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                    <h5 class="card-title">Data siswa</h5>
                <div class="table-responsive">
            <table id="zero_config" class="table table-striped table-bordered" id="mydata">
                <?= $this->session->flashdata('pesan'); ?>
        <thead>
            <tr align="center">
                <th>Id Siswa</th>
                <th>Nama Siswa</th>
                <th>SubID Kota/Kabupaten</th>
                <th>Kecamatan</th>
                <th>Alamat</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($data_siswa as $key => $values){
                    echo "
                        <tr  align='center'>
                        <td>".$data_siswa[$key]->id_siswa."</td>                     
                        <td>".$data_siswa[$key]->nama_siswa."</td>                     
                        <td>".$data_siswa[$key]->kota."</td>                   
                        <td>".$data_siswa[$key]->kecamatan."</td>                   
                        <td>".$data_siswa[$key]->alamat."</td>                                   
                    ";?>
                <td align="center">
                <a class="btn btn-info btn-us" title="ubah" href="<?php echo base_url();?>C_siswa/edit/<?php echo $data_siswa[$key]->id_siswa;?>"><span class="fa fa-edit"></span></a>
                <a class="btn btn-danger btn-us" title="hapus" onclick="return confirm('Ingin menghapus Data?');" href="<?php echo base_url();?>c_siswa/delete/<?php echo $data_siswa[$key]->id_siswa;?>"><span class ="fa fa-trash"></span></a>
                </td>
                <?php echo "</tr>";
                }   
            ?>
        </tbody>
    </table>
    <!--modal input-->
        <p><a class="btn btn-success" data-toggle="modal" data-target="#modal_add_new"><i class="fa fa-plus"></i>Tambah Siswa</a>
        </div>
    </div>
</div>
<footer class="footer text-center">
                All Rights Reserved by Darva. <a href="https://instagram.com/../">Instagram Me</a>.
            </footer>
<div class="modal fade" id="modal_add_new" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="myModalLabel">Tambah Siswa</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url('c_siswa/proses_tambah');?>">
                <div class="modal-body">
 
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Id siswa</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" name="id_siswa" value="SSW<?php echo sprintf("%04s", $id_siswa) ?>" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama siswa</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control" name="nama_siswa" placeholder="Nama Siswa" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Kota/Kabupaten</label>
                        <div class="col-xs-8">
                            <select name="data_kota" id="data_kota"  class="form-control" required>
                                <option value="0">-Pilih-</option>
                            <?php foreach($data_kota as $data){ ?>
                                <option value="<?= $data->id_kota?>"><?= $data->kota; ?></option>
                            <?php } ?>
                             </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Kecamatan</label>
                        <div class="col-xs-8">
                            <select name="data_kecamatan" id="data_kecamatan"  class="form-control" required>
                                <option value="0">-Pilih-</option>
                             </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Alamat</label>
                        <div class="col-xs-8">
                            <textarea name="alamat" class="form-control" placeholder="Alamat" required></textarea>
                        </div>
                    </div>
                </div>
 
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info" name="submit">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
 
            $('#data_kota').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('c_siswa/get_sub_kota');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id_kecamatan+'>'+data[i].kecamatan+'</option>';
                        }
                        $('#data_kecamatan').html(html);
                        
                    }

                });
                return false;
            });
        });
    </script>
    <script src="<?php echo base_url('assets/extra-libs/multicheck/datatable-checkbox-init.js')?>"></script>
    <script src="<?php echo base_url('assets/extra-libs/multicheck/jquery.multicheck.js')?>"></script>
    <script src="<?php echo base_url('assets/extra-libs/DataTables/datatables.min.js')?>"></script>
    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#zero_config').DataTable();
    </script>
</html>