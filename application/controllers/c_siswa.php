<?php

class C_siswa extends CI_controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model(array('m_siswa','m_kecamatan','m_kota'));
		
	}
	
	function index(){
		$dariDB = $this->m_siswa->cekkodebarang();
        $nourut = substr($dariDB, 3, 4);
        $id_skrg = $nourut + 1;
        $data = array('id_siswa' => $id_skrg);
        $data['data_kecamatan'] = $this->m_kecamatan->getKec()->result();
        $data['data_kota'] = $this->m_kota->getKota()->result();
		$data['data_siswa'] = $this->m_siswa->select()->result();
		$this->load->view("Main/siswa",$data);
	}
	function get_sub_kota(){
        $kota = $this->input->post('id',TRUE);
        $data = $this->m_kota->get_sub_kota($kota)->result();
        echo json_encode($data);
    }
	public function proses_tambah(){

		$id_siswa=$_POST['id_siswa'];
		$nama_siswa=$_POST['nama_siswa'];
		$data_kota=$_POST['data_kota'];
		$data_kecamatan=$_POST['data_kecamatan'];
		$alamat=$_POST['alamat'];

            $notif = '';
            $data = array(
                'id_siswa' => $id_siswa,
                'nama_siswa' => $nama_siswa,
                'kota' => $data_kota,
                'kecamatan' => $data_kecamatan,
                'alamat' => $alamat
               
			);
		
		$this->m_siswa->add($data);
		$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				<strong>Succes! </strong>Data Tersimpan.</div>');
		redirect('c_siswa/index');
	}
	public function edit($kode=0){
		$data_siswa=$this->m_siswa->select("where id_siswa='$kode'")->result_array();
		
		$data=array(
			'id_siswa'=>$data_siswa[0]['id_siswa'],
			'nama_siswa'=>$data_siswa[0]['nama_siswa'],
			'data_kota'=>$data_siswa[0]['kota'],
			'data_kecamatan'=>$data_siswa[0]['kecamatan'],
			'alamat'=>$data_siswa[0]['alamat']
		);
		$data['data_kecamatan'] = $this->m_kecamatan->getKec()->result();
        $data['data_kota'] = $this->m_kota->getKota()->result();
		$this->load->view('Main/edit_siswa',$data);
	}
	public function update(){
		$data=array(
			'id_siswa'=>$this->input->post('id_siswa'),
			'nama_siswa'=>$this->input->post('nama_siswa'),
			'kota'=>$this->input->post('data_kota'),
			'kecamatan'=>$this->input->post('data_kecamatan'),
			'alamat'=>$this->input->post('alamat')
			
		);
		$this->m_siswa->ubah($data);
		$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				<strong>Succes! </strong>Data Terupdate.</div>');
		redirect('C_siswa/index');
	}
	public function delete($kode=0){
		$hasil=$this->m_siswa->hapus('data_siswa',array('id_siswa'=>$kode));
		if($hasil==1){
			$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				<strong>Succes! </strong>Data Terhapus.</div>');
			redirect('C_siswa/index');
		}else{
			echo "Data Gagal Terhapus";
		}
	}
}
?>