<?php

class C_kecamatan extends CI_controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model(array('m_kecamatan','m_kota'));
		
	}
	
	function index(){
		//untuk input id otomatis
		$dariDB = $this->m_kecamatan->cekkodebarang();
        $nourut = substr($dariDB, 3, 4);
        $id_skrg = $nourut + 1;
        $data = array('id_kecamatan' => $id_skrg);
        $data['data_kota'] = $this->m_kota->getKota()->result();
		$data['data_kecamatan'] = $this->m_kecamatan->select()->result();
		$this->load->view('Main/kecamatan', $data);
	}
	public function proses_tambah(){

		$id_kecamatan=$_POST['id_kecamatan'];
		$kecamatan=$_POST['kecamatan'];
		$subkota_id=$_POST['subkota_id'];

            $notif = '';
            $data = array(
                'id_kecamatan' => $id_kecamatan,
                'kecamatan' => $kecamatan,
                'subkota_id' =>$subkota_id,

			);

		$this->m_kecamatan->add($data);
		$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				<strong>Succes! </strong>Data Tersimpan.</div>');
		redirect('c_kecamatan/index');
	}
	
	public function edit($kode=0){
		$data_kecamatan=$this->m_kecamatan->select("where id_kecamatan='$kode'")->result_array();

		$data=array(
			'id_kecamatan' => $data_kecamatan[0]['id_kecamatan'],
			'kecamatan'=>$data_kecamatan[0]['kecamatan'],
			'subkota_id'=>$data_kecamatan[0]['subkota_id'],
		);
		$data['data_kota'] = $this->m_kota->getKota()->result();
		$this->load->view('Main/edit_kecamatan',$data);
	}
	public function update(){

		$data=array(
			'id_kecamatan'=>$this->input->post('id_kecamatan'),
			'kecamatan'=>$this->input->post('kecamatan'),
			'subkota_id'=>$this->input->post('subkota_id')
			
		);
		$this->m_kecamatan->ubah($data);
		$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				<strong>Succes! </strong>Data Terupdate.</div>');
		redirect('C_kecamatan/index');
	}
	public function delete($kode=0){
		$hasil=$this->m_kecamatan->hapus('data_kecamatan',array('id_kecamatan'=>$kode));
		if($hasil==1){
			$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				<strong>Succes! </strong>Data Terhapus.</div>');
			redirect('C_kecamatan/index');
		}else{
			echo "Data Gagal Terhapus";
		}
	}
}
?>