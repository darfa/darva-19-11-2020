<?php

class C_kota extends CI_controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model(array('m_kota'));
		
	}
	
	function index(){
		$data['data_kota'] = $this->m_kota->select()->result();
		$this->load->view('Main/kota', $data);
	}
	public function proses_tambah(){

		$id_kota=$_POST['id_kota'];
		$kota=$_POST['kota'];

            $notif = '';
            $data = array(
                'id_kota' => $id_kota,
                'kota' => $kota,

			);
			
		$this->m_kota->add($data);
		$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				<strong>Succes! </strong>Data Tersimpan.</div>');
		redirect('c_kota/index');
	}
	
	public function edit($kode=0){
		$data_kota=$this->m_kota->select("where id_kota='$kode'")->result_array();

		$data=array(
			'id_kota' => $data_kota[0]['id_kota'],
			'kota'=>$data_kota[0]['kota'],
		);
		$this->load->view('Main/edit_kota',$data);
	}
	public function update(){

		$data=array(
			'id_kota'=>$this->input->post('id_kota'),
			'kota'=>$this->input->post('kota')
			
		);
		$this->m_kota->ubah($data);
		$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				<strong>Succes! </strong>Data Terupdate.</div>');
		redirect('C_kota/index');
	}
	public function delete($kode=0){
		$hasil=$this->m_kota->hapus('data_kota',array('id_kota'=>$kode));
		if($hasil==1){
			$this->session->set_flashdata('pesan','<div class="alert alert-success alert-dismissible">
    				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    				<strong>Succes! </strong>Data Terhapus.</div>');
			redirect('C_kota/index');
		}else{
			echo "Data Gagal Terhapus";
		}
	}
}
?>